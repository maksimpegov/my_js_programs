//Покимон-злодей
var text = "Вы гуляли и ловили покемонов, но неожидонно встретили покемона-людоеда!";
text += "Что вы сделаете? Выберете ДРАТСЯ,ЗАПЛАТИТЬ или УБЕЖАТЬ?";
                        
var troll = prompt(text).toUpperCase();

switch(troll) {
  case 'ДРАТСЯ':
    fight();
    break;
  case 'ЗАПЛАТИТЬ':
    var money = prompt("All right, we'll pay the troll. Do you have any money (YES or NO)?").toUpperCase();
    var dollars = prompt("Is your money in Troll Dollars?").toUpperCase();
    if(money === 'YES' && dollars === 'YES') {
      console.log("Great! You pay the troll and continue on your merry way.");
    } else {
      console.log("Dang! This troll only takes Troll Dollars. You get whomped!");
    }
    break;
  case 'УБЕЖАТЬ':
    var fast = prompt("Let's book it! Are you fast (YES or NO)?").toUpperCase();
    var headStart = prompt("Did you get a head start?").toUpperCase();
    if(fast === 'YES' || headStart === 'YES') {
      console.log("You got away--barely! You live to stroll through the forest another day.");
    } else {
      console.log("You're not fast and you didn't get a head start? You never had a chance! The troll eats you.");
    }
    break;
  default:
    console.log("I didn't understand your choice. Hit Run and try again, this time picking FIGHT, PAY, or RUN!");
}

var fight = function(){
    var lifes = 5
    var slaying = true;
var youHit = Math.floor(Math.random() * 2);
var damageThisRound = Math.floor(Math.random()*5 + 1);
var totalDamage = 0
while(slaying){  //Начало сражения
    if(youHit){ 
        totalDamage = totalDamage + damageThisRound;
        console.log('Вы попали! У покемона осталось ' + (lifes - totalDamage)*100/lifes +' % жизни');
        if(totalDamage>=lifes ){
            console.log("Вы победили покемона");
            slaying = false;
        } // Победа
    } //Мы попали
    else { 
        console.log('Покемон съел тебя.')
        slaying = false;
    } //Дракон победил
}   
}